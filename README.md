# PostgreSQL 11
Bastille template to bootstrap PostgresSQL 11.7

## Status
[![pipeline status](https://gitlab.com/jaredwolff/postgresql11-server-11.7/badges/master/pipeline.svg)](https://gitlab.com/jaredwolff/postgresql11-server-11.7/commits/master)

## Bootstrap
```shell
bastille bootstrap https://gitlab.com/jaredwolff/postgresql11-server-11.7
```

## Usage
```shell
bastille template TARGET jaredwolff/postgresql11-server-11.7
```

Template curtesy of Christer Edwards!